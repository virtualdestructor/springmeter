/*SpringMeter
      Copyright (C) 2014  Barnabás Czémán & Rómeó Ervin Fukász

      This program is free software: you can redistribute it and/or modify
      it under the terms of the GNU General Public License as published by
      the Free Software Foundation, either version 3 of the License, or
      (at your option) any later version.

      This program is distributed in the hope that it will be useful,
      but WITHOUT ANY WARRANTY; without even the implied warranty of
      MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
      GNU General Public License for more details.

      You should have received a copy of the GNU General Public License
      along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
package my.SpringMeter;

import android.app.Activity;
import android.content.Intent;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.os.Bundle;
import android.widget.CompoundButton;
import android.widget.TextView;
import android.widget.ToggleButton;

public class MainActivity extends Activity implements SensorEventListener{

    private SensorManager sensorManager;
    boolean isG;

    TextView data;
    ToggleButton record;
    ToggleButton g;
    GlobalData appData;

    /**
     * @brief Az activity índulásakor fut le.
     * @param savedInstanceState
     **/
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.main);


        data = (TextView) findViewById(R.id.data);
        record = (ToggleButton) findViewById(R.id.record);
        g=(ToggleButton) findViewById(R.id.g);
        appData = (GlobalData)getApplicationContext();

        appData.clearData();


        //Toggle button events
        record.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (!isChecked && appData.aSize() != 0) {
                    //Adatok szürése
                    appData.filterData();
                    //Sebesség és elmozdulsá számítása
                    appData.computeVelocity();
                    appData.computeDisplacement();
                    //Grafikon elíndítása
                    Intent intent = new Intent(MainActivity.this, PlotActivity.class);
                    startActivity(intent);
                }
            }
        });

        //Toggle button events
        g.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (!isChecked) {
                          isG=false;
                }else{
                    isG=true;
                }
            }
        });

        sensorManager = (SensorManager) getSystemService(SENSOR_SERVICE);

        //Szenzor listner beregisztrálása
        sensorManager.registerListener(this,
                sensorManager.getDefaultSensor(Sensor.TYPE_ACCELEROMETER),
                SensorManager.SENSOR_DELAY_GAME);
    }


    /**
     * @param outState
     */
    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
    }

    /**
     * @brief Ha váltózott a szenzor értéke akkor meghívódik
     * @param sensorEvent   szenzor eseményei
     */
    @Override
    public void onSensorChanged(SensorEvent sensorEvent) {
        if (sensorEvent.sensor.getType() == Sensor.TYPE_ACCELEROMETER) {
            if (record.isChecked() && sensorEvent.accuracy != SensorManager.SENSOR_STATUS_UNRELIABLE
                    && (sensorEvent.accuracy == SensorManager.SENSOR_STATUS_ACCURACY_MEDIUM ||
                        sensorEvent.accuracy == SensorManager.SENSOR_STATUS_ACCURACY_HIGH)) {
                //Ha Y függöleges akkor levonjuk a g-t (g=9,81)
                if(isG==false) {
                    appData.aAdd(sensorEvent.values[1] - 9.81f);
                }
                //Ha vizszíntes model van akkor az Y tengelyen nem jelenik meg a g
                else{
                    appData.aAdd(sensorEvent.values[1]);
                }
                //Idő tárolása
                appData.tAdd(sensorEvent.timestamp);
                //Szöveg beállítása
                data.setText("DATA:" + appData.aGetAt(appData.aSize()-1));
            }
        }
    }

    /**
     * @brief Visszatéréskor törli az adatokat.
     */
    @Override
    protected void onResume() {
        super.onResume();
        appData.clearData();
    }

    /**
     * @brief Csak a kötelező felüldefiniálás miatt kell.
     * @param sensor
     * @param i
     */
    @Override
    public void onAccuracyChanged(Sensor sensor, int i) {}
}