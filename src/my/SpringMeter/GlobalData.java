/*SpringMeter
      Copyright (C) 2014  Barnabás Czémán & Rómeó Ervin Fukász

      This program is free software: you can redistribute it and/or modify
      it under the terms of the GNU General Public License as published by
      the Free Software Foundation, either version 3 of the License, or
      (at your option) any later version.

      This program is distributed in the hope that it will be useful,
      but WITHOUT ANY WARRANTY; without even the implied warranty of
      MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
      GNU General Public License for more details.

      You should have received a copy of the GNU General Public License
      along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
package my.SpringMeter;

import android.app.Application;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class GlobalData extends Application {

    public GlobalData(){
        a = new ArrayList<Float>();
        v = new ArrayList<Float>();
        x = new ArrayList<Float>();
        t = new ArrayList<Long>();
        temp = new ArrayList<Float>();
    }

    private ArrayList<Float> a, v, x, temp;
    private ArrayList<Long> t;

    public void clearData(){
        a.clear();
        v.clear();
        x.clear();
        t.clear();
        temp.clear();
    }

    public void aAdd(Float n){
        a.add(n);
    }
    public int aSize(){
        return a.size();}
    public Number aGetAt(int i){return a.get(i);}
    public List<Float>aGetList(){return a;}
    public void aClear(){a.clear();}

    public void vClear(){v.clear();}
    public List<Float>vGetList(){return v;}

    public void xClear(){x.clear();}

    public void tAdd(long l){
        t.add(l);
    }
    public void tClear(){
        t.clear();
    }

    public void filterData(){
        int i,n;

        Float avg;
        //ha van elem a timestampek között (ugyanennyi lesz az adatokból is)
        while (!t.isEmpty() && !a.isEmpty()) {
            // ts legyen a jelenlegi első timestamp
            float ts = t.get(0);
            // amíg az adott elemből et kivonva max 100ms-re vagyunk
            //ez azért ennyi, mert egy ms 10^6 ns, szal 8 nulla kell
            // azt is ellenőrizzük, hogy legyen annyi eleme a listáknak amennyit már találtunk, különben kész vagyunk
            for (i = 0, n = 1, avg = 0.0f; (t.get(i) - ts) < 20 && n < t.size(); i++, n++) {
                //az átlaghoz hozzáadjuk az adat értékét és a tartomány adatainak számát (n) növeljük
                avg += a.get(i);
            }
            //az átlag értékét a darabszámmal való osztás adja
            avg /= n;
            // kicsit szűrjük a zajt a 0 értékeknél
            if (avg < 0.1f && avg > -0.1f) avg = 0.0f;
            // az ideiglenes listába betesszük a kapott értéket, ez lesz az adott 100ms tartományhoz tartozó pont
            temp.add(avg);
            //kitöröljük a felhasznált adatokat a listák elejéről
            for (i = 0; i < n; i++) {
                a.remove(0);
                t.remove(0);
            }
        }
        a.clear(); //biztonsági törlése az a listának
        t.clear(); //erre már nagyon nincs szükség, elvileg üres de mégis :D
        for (i = 0; i < temp.size(); i++) a.add(temp.get(i));//átpakoljuk a kapott listát az a-ba
        temp.clear(); //a tempet meg kitakarítjuk
    }

    public void computeVelocity(){



        //0 közepűre hozás
        float max,min;
        max= Collections.max(a);
        min= Collections.min(a);

        for (int i=0;i < a.size(); i++) {
            a.set(i,a.get(i)-((min+max)/2));

        }



        // annyi adat lesz sebességből amennyi gyorsulás is volt
        v.add((float) (a.get(0) * 0.02));





       for (int i = 1; i < a.size(); i++) {
           v.add((float) ((a.get(i) + a.get(i - 1) )/2 * 0.02)+v.get(i-1));



       }

        //0 közepűre hozás
        float vmax,vmin;
        vmax= Collections.max(v);
        vmin= Collections.min(v);

        for (int i=0;i < a.size(); i++) {
            v.set(i,v.get(i)-((vmin+vmax)/2));

        }

    }

    /**
     * @brief A kitérés számítása rugómozgásra mivel a rugó harmonikus rezgő mozog
     */
    public void computeDisplacement(){

        float vmax,vmin,amax,amin;
        vmax= Collections.max(v);
        vmin= Collections.min(v);

        amax= Collections.max(a);
        amin= Collections.min(a);

        float omega=(amax-amin)/(vmax-vmin);

        Toast.makeText(this, String.format("A periódusidő: %f sec", 2 * 3.141592654f/omega), Toast.LENGTH_SHORT).show();




        for(int i=0;i<a.size();i++)
        {
            x.add((float)(a.get(i)/omega/omega*(-1.0f)*100.0f));    //*100 váltja át cm-be az elmozdulást.

        }



    }

    /**
     *
     * @return   Visszad egy listát
     */
    public List<Float> xGetList() {
        return x;
    }
}
