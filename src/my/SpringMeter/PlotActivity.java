/*SpringMeter
      Copyright (C) 2014  Barnabás Czémán & Rómeó Ervin Fukász

      This program is free software: you can redistribute it and/or modify
      it under the terms of the GNU General Public License as published by
      the Free Software Foundation, either version 3 of the License, or
      (at your option) any later version.

      This program is distributed in the hope that it will be useful,
      but WITHOUT ANY WARRANTY; without even the implied warranty of
      MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
      GNU General Public License for more details.

      You should have received a copy of the GNU General Public License
      along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
package my.SpringMeter;

import android.app.Activity;
import android.os.Bundle;
import com.androidplot.xy.*;


public class PlotActivity extends Activity {

    /**
     * @brief Plot activity létre jöttekor fut le.
     * @param savedInstanceState
     */
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        appData = (GlobalData) getApplicationContext();

        setContentView(R.layout.plot);

        // Függvény rajzoló lekérdezése
        plot = (XYPlot) findViewById(R.id.mySimpleXYPlot);

        // Gyorsulás ábrázolása
        XYSeries series1 = new SimpleXYSeries(
                appData.aGetList(),          // SimpleXYSeries takes a List so turn our array into a List
                SimpleXYSeries.ArrayFormat.Y_VALS_ONLY, // Y_VALS_ONLY means use the element index as the x value
                "acceleration");                             // Set the display title of the series

        // Sebesség ábrázolása
         XYSeries series2 = new SimpleXYSeries(
                 appData.vGetList(),
                 SimpleXYSeries.ArrayFormat.Y_VALS_ONLY,
                 "velocity");

        // Elmozdulás ábrázolása
        XYSeries series3 = new SimpleXYSeries(
                appData.xGetList(),
                SimpleXYSeries.ArrayFormat.Y_VALS_ONLY,
                "position");





        //Ponotk címkéjének formázása
        PointLabelFormatter plf=new PointLabelFormatter(){

            public android.graphics.Paint getTextPaint(){

                android.graphics.Paint p=new android.graphics.Paint();

                p.setColor(123);

                return p;
            }

        };


        //Vonal és a pontok formázása
        LineAndPointFormatter series1Format = new LineAndPointFormatter();
        series1Format.setPointLabelFormatter(plf);
        series1Format.configure(getApplicationContext(),
                R.xml.line_point_formatter_with_plf1);

        //Pontsorozat hozzáadása
        plot.addSeries(series1, series1Format);

        //Vonal és a pontok formázása
        LineAndPointFormatter series2Format = new LineAndPointFormatter();
        series2Format.setPointLabelFormatter(plf);
        series2Format.configure(getApplicationContext(),
               R.xml.line_point_formatter_with_plf2);
        //Pontsorozat hozzáadása
        plot.addSeries(series2, series2Format);

        //Vonal és a pontok formázása
        LineAndPointFormatter series3Format = new LineAndPointFormatter();
        series3Format.setPointLabelFormatter(plf);
        series3Format.configure(getApplicationContext(),
                R.xml.line_point_formatter_with_plf3);
        //Pontsorozat hozzáadása
        plot.addSeries(series3, series3Format);

        //Skála
        plot.setTicksPerRangeLabel(3);
        plot.getGraphWidget().setDomainLabelOrientation(-45);
        plot.setRangeBoundaries(-
             100,100,BoundaryMode.FIXED);
    }


    //Tag változók
    private XYPlot plot;
    GlobalData appData;
}